<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tbl_book extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'username' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                                
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '75',
                        ),
                        'stts' => array(
                                  'type' => 'VARCHAR',
                                'constraint' => '10',
                           
                        ),
            


                ));
                $this->dbforge->add_key('username', TRUE);
                $this->dbforge->create_table('tbl_book');
        }

        public function down()
        {
                $this->dbforge->drop_table('tbl_login');
        }
}